package core;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.net.URL;

public class DriverThread {

    private static final Logger logger = LoggerFactory.getLogger(DriverThread.class);

    public WebDriver driver;

    public void initialiseDriver() {

        BrowserType type = determineDriverType();

        startDriverRemotely(type);

        determineDriverType();
    }

    public WebDriver getDriver() {
        return driver;
    }

    private BrowserType determineDriverType() {
        BrowserType type = BrowserType.CHROME;
        try {
            type = BrowserType.valueOf(System.getProperty("browser.type").toUpperCase());
        } catch (IllegalArgumentException | NullPointerException e) {
            System.out.println("Not recognize browser: please use chrome or firefox");
        }
        return type;
    }

    private void startDriverRemotely(BrowserType type) {
        DesiredCapabilities capabilities;
        switch (type) {
            case CHROME:
                capabilities = DesiredCapabilities.chrome();
                break;
            case FIREFOX:
                capabilities = DesiredCapabilities.firefox();
                break;
            default:
                capabilities = DesiredCapabilities.chrome();
        }
        try {
            URL url = new URL(System.getProperty("grid.url"));
            driver = new RemoteWebDriver(url, capabilities);
            ((RemoteWebDriver) driver).setFileDetector(new LocalFileDetector());
        } catch (MalformedURLException e) {
            logger.error(e.getMessage());
        }
    }
}
