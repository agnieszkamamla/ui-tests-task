package core;

import org.openqa.selenium.WebDriver;

public class DriverFactory {

    public static ThreadLocal<DriverThread> driverThread;

    public static void initialiseDriver() {
        driverThread = ThreadLocal.withInitial(() -> {
            DriverThread driverThread = new DriverThread();
            driverThread.initialiseDriver();
            return driverThread;
        });
    }

    public static void clearCookies() {
        driverThread.get().getDriver().manage().deleteAllCookies();
    }

    public static void quitDriver() {
        driverThread.get().getDriver().quit();
    }

    public static WebDriver getDriver() {
        return driverThread.get().getDriver();
    }
}
