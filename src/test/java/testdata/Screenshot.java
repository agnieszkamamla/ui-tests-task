package testdata;

import com.google.common.io.Files;
import utils.IoUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static testdata.ConstTestData.*;

public enum Screenshot {

    PNG_TEST(SCREENSHOT_PNG_TITLE, SCREENSHOT_TAGS, SCREENSHOT_DESCRIPTION, PATH_TO_PNG_FILE, SCREENSHOT_PNG_NAME),

    JPG_TEST(SCREENSHOT_JPG_TITLE, SCREENSHOT_TAGS, SCREENSHOT_DESCRIPTION, PATH_TO_JPG_FILE, SCREENSHOT_JPG_NAME),

    PDF_TEST(SCREENSHOT_PDF_TITLE, SCREENSHOT_TAGS, SCREENSHOT_DESCRIPTION, PATH_TO_PDF_FILE, SCREENSHOT_PDF_NAME);

    private String title;

    private List<String> tags;

    private String path;

    private String fileName;

    private String description;

    public String getTitle() {
        return title;
    }

    public List<String> getTags() {
        return tags;
    }

    public String getDescription() {
        return description;
    }

    Screenshot(String title, List<String> tags, String description, String path, String fileName) {
        this.title = title;
        this.tags = tags;
        this.path = path;
        this.fileName = fileName;
        this.description = description;
    }

    public String getAttachmentAbsolutePath(ClassLoader classLoader) {
        File res;
        try (InputStream is = classLoader.getResourceAsStream(path)) {
            res = new File(Files.createTempDir(), fileName);
            IoUtils.copy(is, new FileOutputStream(res));

        } catch (IOException e) {
            throw new RuntimeException("Cannot find path to attachment");
        }
        return res.getAbsolutePath();
    }
}
