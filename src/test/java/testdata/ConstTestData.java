package testdata;

import com.google.common.collect.Lists;

import java.util.Arrays;
import java.util.List;

public interface ConstTestData {

    String MAIN_URL = "https://dribbble.com";

    String USER_NAME = System.getProperty("user.name");

     String USER_PASSWORD = System.getProperty("user.password");

     String SUCCESS_MESSAGE = "Your screenshot has been published :-)";

     String NO_RESULTS_MESSAGE = "No results match";

     String LOADING_ERROR_MESSAGE = "is not a valid type. Valid file types are: JPG, PNG, GIF.";

     String EMPTY_TITLE_ERROR_MESSAGE = "Title can't be blank";

     String PATH_TO_PNG_FILE = "testpng.png";

     String PATH_TO_JPG_FILE = "testjpg.jpg";

     String PATH_TO_PDF_FILE = "testpdf.pdf";

     String SCREENSHOT_PNG_NAME = "testpng.png";

     String SCREENSHOT_JPG_NAME = "testjpg.jpg";

     String SCREENSHOT_PDF_NAME = "testpdf.pdf";

     String SCREENSHOT_PNG_TITLE = "Travel book test png";

     String SCREENSHOT_JPG_TITLE = "Travel book test jpg";

     String SCREENSHOT_PDF_TITLE = "Travel book test pdf";

     List<String> SCREENSHOT_TAGS = Arrays.asList("travel book", "travel planner", "explorer");

     String SCREENSHOT_DESCRIPTION = "My first travel app";

     List<String> SEARCHED_TAGS = Lists.newArrayList("travel", "planner");

     String SEARCHED_AUTHOR = "Netguru";

     String SEARCHED_TITLE = "NYC";

     String SEARCHED_RANDOM_STRING = "shgdjshgdsgjgsjhd";

     String EMPTY = "";
}
