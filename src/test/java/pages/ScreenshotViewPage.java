package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import utils.CommonTestUtils;

import java.util.List;

public class ScreenshotViewPage {

    private By titleLocator = By.className("shot-title");

    private By tagsListLocator = By.className("shot-tags");

    private By tagLocator = By.className("tag");

    private By authorLocator = By.className("shot-byline-user");

    public WebElement getScreenshotTitle() {
        return CommonTestUtils.findElementWithWait(titleLocator);
    }

    public WebElement getTagList() {
        return CommonTestUtils.findElementWithWait(tagsListLocator);
    }

    public WebElement getAuthor() {
        return CommonTestUtils.findElementWithWait(authorLocator);
    }

    public List<WebElement> getTags() {
        return CommonTestUtils.findElementsWithWait(getTagList(), tagLocator);
    }
}
