package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static utils.CommonTestUtils.findElementWithWait;

public class LoadScreenshotPage {

    private String publishedScreenshotUrlPart = "shots";

    private String cropAndContinueButtonText = "Crop + continue";

    private By loadScreenshotIconLocator = By.cssSelector("input#screenshot_file");

    private By cropAndContinueButtonLocator = By.cssSelector("a.form-sub");

    private By titleFieldLocator = By.id("screenshot_title");

    private By tagsFieldLocator = By.id("screenshot_tag_list-selectized");

    private By descriptionFieldLocator = By.id("screenshot_description");

    private By publishButtonLocator = By.cssSelector("input.form-sub");

    private By publishSuccessAlertLocator = By.cssSelector("div.notice.success");

    private By publishedScreenshotTitleLocator = By.cssSelector("h1.shot-title");

    private By screenshotPreviewLocator = By.className("single-img");

    public By loadingErrorLocator = By.id("errorExplanation");

    public String getPublishedScreenshotUrlPart() {
        return publishedScreenshotUrlPart;
    }

    public String getCropAndContinueButtonText() {
        return cropAndContinueButtonText;
    }

    public By getScreenshotPreviewLocator() {
        return screenshotPreviewLocator;
    }

    public WebElement getLoadScreenshotIcon() {
        return findElementWithWait(loadScreenshotIconLocator);
    }

    public WebElement getCropAndCountButton() {
        return findElementWithWait(cropAndContinueButtonLocator);
    }

    public WebElement getTitleField() {
        return findElementWithWait(titleFieldLocator);
    }

    public WebElement getTagsField() {
        return findElementWithWait(tagsFieldLocator);
    }

    public WebElement getDescriptionField() {
        return findElementWithWait(descriptionFieldLocator);
    }

    public WebElement getPublishButton() {
        return findElementWithWait(publishButtonLocator);
    }

    public WebElement getPublishSuccessAlert() {
        return findElementWithWait(publishSuccessAlertLocator);
    }

    public WebElement getPublishedScreenshotTitle() {
        return findElementWithWait(publishedScreenshotTitleLocator);
    }

    public WebElement getLoadingError() {
         return findElementWithWait(loadingErrorLocator);
    }
}
