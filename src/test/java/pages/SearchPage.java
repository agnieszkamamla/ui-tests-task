package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import utils.CommonTestUtils;

import java.util.List;

public class SearchPage {

    private String urlPart = "search?q=";

    private By resultLocator = By.className("dribbble-over");

    private By resultsMainPageLocator = By.id("main");

    private By resultsListLocator = By.cssSelector("ol.dribbbles.group");

    private By emptyResultsListMessage = By.cssSelector("div.null.null-message");

    public By getResultsMainPageLocator() {
        return resultsMainPageLocator;
    }

    public WebElement getEmptyResultsListMessage() {
        return CommonTestUtils.findElementWithWait(emptyResultsListMessage);
    }

    public List<WebElement> getResults() {
        CommonTestUtils.checkIfUrlContains(urlPart);
        CommonTestUtils.waitUntilWebElementIsPresent(resultsListLocator);
        return CommonTestUtils.findElementsWithWait(resultLocator);
    }
}
