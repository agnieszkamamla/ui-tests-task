package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static utils.CommonTestUtils.findElementWithWait;

public class LoginPage {

    private static final String loginPagePartUrl = "session";

    private By loginFieldLocator = By.id("login");

    private By passwordFieldLocator = By.id("password");

    private By signInButton = By.cssSelector("input.button");

    public String getLoginPagePartUrl() {
        return loginPagePartUrl;
    }

    public By getLoginFieldLocator() {
        return loginFieldLocator;
    }

    public WebElement getPasswordField() {
        return findElementWithWait(passwordFieldLocator);
    }

    public WebElement getSignInButton() {
        return findElementWithWait(signInButton);
    }
}
