package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static utils.CommonTestUtils.findElementWithWait;

public class NavigationPanel {

    private By signInActionButtonLocator = By.id("t-signin");

    private By searchFieldLocator = By.cssSelector("form#search > input");

    private By uploadButtonLocator = By.id("t-upld");

    public WebElement getSearchField() {
        return findElementWithWait(searchFieldLocator);
    }

    public WebElement getUploadButton() {
        return findElementWithWait(uploadButtonLocator);
    }

    public WebElement getSignInActionButton() {
        return findElementWithWait(signInActionButtonLocator);
    }
}
