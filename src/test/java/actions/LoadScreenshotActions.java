package actions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pages.LoadScreenshotPage;
import utils.CommonTestUtils;

import java.util.List;
import java.util.stream.Collectors;

public class LoadScreenshotActions {

    private static final Logger logger = LoggerFactory.getLogger(LoadScreenshotActions.class);

    private LoadScreenshotPage loadScreenshotPage;

    public LoadScreenshotActions() {
        loadScreenshotPage = new LoadScreenshotPage();
    }

    public LoadScreenshotActions loadFile(String filePath) {
        logger.info("Load file {}", filePath);
        loadScreenshotPage.getLoadScreenshotIcon().sendKeys(filePath);
        return this;
    }

    public LoadScreenshotActions clickOnCropAndContinueButton() {
        CommonTestUtils.waitUntilWebElementContainsText(loadScreenshotPage.getCropAndCountButton(), loadScreenshotPage.getCropAndContinueButtonText());
        logger.info("Click on 'Crop and continue' button");
        CommonTestUtils.clickElementWithWait(loadScreenshotPage.getCropAndCountButton());
        if (loadScreenshotPage.getCropAndCountButton().getText().contains(loadScreenshotPage.getCropAndContinueButtonText())) {
            CommonTestUtils.clickElementWithWait(loadScreenshotPage.getCropAndCountButton());
        }
        return this;
    }

    public LoadScreenshotActions typeScreenshotTitle(String title) {
        logger.info("Type screenshot title {}", title);
        CommonTestUtils.waitUntilWebElementIsPresent(loadScreenshotPage.getScreenshotPreviewLocator());
        CommonTestUtils.sendKeysWithClearField(loadScreenshotPage.getTitleField(), title);
        return this;
    }

    public LoadScreenshotActions typeScreenShotTags(List<String> tags) {
        logger.info("Type screenshot tags {}", tags);
        String tagsToType = tags.stream().map(tag -> tag + ", ").collect(Collectors.joining());
        CommonTestUtils.sendKeys(loadScreenshotPage.getTagsField(), tagsToType);
        return this;
    }

    public LoadScreenshotActions typeScreenshotDescription(String description) {
        logger.info("Type screenshot description {}", description);
        CommonTestUtils.sendKeys(loadScreenshotPage.getDescriptionField(), description);
        return this;
    }

    public LoadScreenshotActions clickOnPublishDraftButton() {
        logger.info("Click on 'Publish Draft' button'");
        CommonTestUtils.clickElementWithWait(loadScreenshotPage.getPublishButton());
        return this;
    }

    public String getLoadScreenshotAlertMessage() {
        logger.info("Get publish alert message");
        CommonTestUtils.checkIfUrlContains(loadScreenshotPage.getPublishedScreenshotUrlPart());
        return loadScreenshotPage.getPublishSuccessAlert().getText();
    }

    public String getPublishedScreenshotTitle() {
        logger.info("Get published screenshot title");
        CommonTestUtils.checkIfUrlContains(loadScreenshotPage.getPublishedScreenshotUrlPart());
        return loadScreenshotPage.getPublishedScreenshotTitle().getText();
    }

    public boolean checkIfErrorIsDisplayed() {
        logger.info("Check if error message is displayed");
        return loadScreenshotPage.getLoadingError().isDisplayed();
    }

    public String getErrorMessage() {
        logger.info("Get error message text");
        return loadScreenshotPage.getLoadingError().getText();
    }

}
