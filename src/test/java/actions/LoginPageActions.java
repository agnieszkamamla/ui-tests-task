package actions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pages.LoginPage;
import utils.CommonTestUtils;

import static utils.CommonTestUtils.checkIfUrlContains;

public class LoginPageActions {

    private static final Logger logger = LoggerFactory.getLogger(LoginPageActions.class);

    private LoginPage loginPage;

    public LoginPageActions() {
        loginPage = new LoginPage();
    }

    public LoginPageActions typeUserName(String userName) {
        logger.info("Type user name {}", userName);
        checkIfUrlContains(loginPage.getLoginPagePartUrl());
        CommonTestUtils.sendKeysWithWait(loginPage.getLoginFieldLocator(), userName);
        return this;
    }

    public LoginPageActions typePassword(String password) {
        logger.info("Type user password");
        CommonTestUtils.sendKeys(loginPage.getPasswordField(), password);
        return this;
    }

    public NavigationPanelActions clickLoginButton() {
        logger.info("Click on 'Sign in' button");
        CommonTestUtils.clickElementWithWait(loginPage.getSignInButton());
        return new NavigationPanelActions();
    }
}
