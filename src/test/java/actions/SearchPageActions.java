package actions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pages.SearchPage;
import utils.CommonTestUtils;

import static org.junit.Assert.fail;


public class SearchPageActions {

    private static final Logger logger = LoggerFactory.getLogger(SearchPageActions.class);

    private SearchPage searchPage;

    public SearchPageActions() {
        searchPage = new SearchPage();
    }

    public ScreenshotViewPageActions clickOnFoundedScreenshot() {
        logger.info("Search screenshot by search phrase and click on first result");
        CommonTestUtils.waitUntilWebElementIsPresent(searchPage.getResultsMainPageLocator());
        if (!searchPage.getResults().isEmpty()) {
            searchPage.getResults().get(0).click();
        } else {
            fail("There's no screenshot with selected author/title/tags");
        }
        return new ScreenshotViewPageActions();
    }

    public String getSearchResultMessage() {
        logger.info("Get search result message");
        return searchPage.getEmptyResultsListMessage().getText();
    }

}
