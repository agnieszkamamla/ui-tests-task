package actions;

import core.DriverFactory;
import org.openqa.selenium.WebDriverException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BaseActions {

    private static final Logger logger = LoggerFactory.getLogger(BaseActions.class);

    public BaseActions startTest() {
        logger.info("Initialise driver");
        DriverFactory.initialiseDriver();
        return this;
    }

    public void goToUrl(String url) {
        logger.info("Go to main url {}", url);
        DriverFactory.getDriver().get(url);
    }

    public void goToMainPage(String url) {
        logger.info("Go to main page");
        DriverFactory.getDriver().navigate().to(url);
    }

    public void quitTest() {
        logger.info("Clear cookies and quit driver");
        DriverFactory.clearCookies();
        DriverFactory.quitDriver();
    }

    public BaseActions maximizeWindow() {
        // this try catch block has to be added because of chromedriver exception in few versions
        try {
            logger.info("Maximize window");
            DriverFactory.getDriver().manage().window().maximize();
        } catch (WebDriverException e) {
            logger.error(e.getMessage());
        }
        return this;
    }
}
