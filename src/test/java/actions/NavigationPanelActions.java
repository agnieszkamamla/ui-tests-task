package actions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pages.NavigationPanel;
import utils.CommonTestUtils;

import java.util.List;
import java.util.stream.Collectors;

public class NavigationPanelActions {

    private static final Logger logger = LoggerFactory.getLogger(NavigationPanelActions.class);

    private NavigationPanel navigationPanel;

    public NavigationPanelActions() {
        navigationPanel = new NavigationPanel();
    }

    public LoginPageActions clickLoginButton() {
        logger.info("Click on 'Sign in' button");
        CommonTestUtils.clickElementWithWait(navigationPanel.getSignInActionButton());
        return new LoginPageActions();
    }

    public LoadScreenshotActions clickOnLoadButton() {
        logger.info("Click on 'Upload' button");
        CommonTestUtils.clickElementWithWait(navigationPanel.getUploadButton());
        return new LoadScreenshotActions();
    }

    public SearchPageActions findScreenshotsByTags(List<String> tags) {
        logger.info("Search by tags");
        String textToType = tags.stream().map(tag -> tag + " ").collect(Collectors.joining());
        CommonTestUtils.sendKeysWithEnterConfirm(navigationPanel.getSearchField(), textToType);
        return new SearchPageActions();
    }

    public SearchPageActions findScreenshotsByOneCriterion(String criterion) {
        logger.info("Search by one criterion {}", criterion);
        CommonTestUtils.sendKeysWithEnterConfirm(navigationPanel.getSearchField(), criterion);
        return new SearchPageActions();
    }


}
