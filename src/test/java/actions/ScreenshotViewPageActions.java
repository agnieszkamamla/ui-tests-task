package actions;

import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pages.ScreenshotViewPage;

import java.util.List;
import java.util.stream.Collectors;

public class ScreenshotViewPageActions {

    private static final Logger logger = LoggerFactory.getLogger(ScreenshotViewPageActions.class);

    private ScreenshotViewPage screenshotViewPage;

    public ScreenshotViewPageActions() {
        screenshotViewPage = new ScreenshotViewPage();
    }

    public boolean checkIfAuthorIsVisible() {
        logger.info("Check if author of the screenshot is displayed");
        return screenshotViewPage.getAuthor().isDisplayed();
    }

    public String getAuthor() {
        logger.info("Get screenshot 'Author' name");
        return screenshotViewPage.getAuthor().getText();
    }

    public boolean checkIfTitleIsDisplayed() {
        logger.info("Check if title of the screenshot is displayed");
        return screenshotViewPage.getScreenshotTitle().isDisplayed();
    }

    public String getTitle() {
        logger.info("Get screenshot 'Title'");
        return screenshotViewPage.getScreenshotTitle().getText();
    }

    public List<String> getTags() {
        logger.info("Check tags list assigned to the screenshot");
        return screenshotViewPage.getTags().stream().map(WebElement::getText).collect(Collectors.toList());
    }


}
