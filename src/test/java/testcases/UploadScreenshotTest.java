package testcases;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static testdata.ConstTestData.*;
import static testdata.Screenshot.*;

public class UploadScreenshotTest {

    private static TestBase testBase;

    @BeforeClass
    public static void startTest() {
        testBase = new TestBase();
        testBase.setUp();
    }

    @Before
    public void goToMainPage() {
        testBase.getBaseActions().goToMainPage(MAIN_URL);
    }

    @AfterClass
    public static void tearDown() {
        testBase.tearDown();
    }

    /* In this test popular tags are used to check if there will be any results.
    * On test environment this kind of data feed should be done before tests,
    * it's not necessary to add screenshot with Selenium usage.
    * */

    @Test
    public void uploadNewPngScreenshotTest() {
        testBase.getNavigationPanelActions()
                .clickOnLoadButton()
                .loadFile(PNG_TEST.getAttachmentAbsolutePath(testBase.getClassLoader()))
                .clickOnCropAndContinueButton()
                .typeScreenshotTitle(PNG_TEST.getTitle())
                .typeScreenShotTags(PNG_TEST.getTags())
                .typeScreenshotDescription(PNG_TEST.getDescription())
                .clickOnPublishDraftButton();

        assertThat(testBase.getLoadScreenshotActions().getLoadScreenshotAlertMessage(), is(equalTo(SUCCESS_MESSAGE)));
        assertThat(testBase.getLoadScreenshotActions().getPublishedScreenshotTitle(), is(equalTo(SCREENSHOT_PNG_TITLE)));
    }

    @Test
    public void uploadNewJpgScreenshotTest() {
        testBase.getNavigationPanelActions()
                .clickOnLoadButton()
                .loadFile(JPG_TEST.getAttachmentAbsolutePath(testBase.getClassLoader()))
                .clickOnCropAndContinueButton()
                .typeScreenshotTitle(JPG_TEST.getTitle())
                .typeScreenShotTags(JPG_TEST.getTags())
                .typeScreenshotDescription(JPG_TEST.getDescription())
                .clickOnPublishDraftButton();

        assertThat(testBase.getLoadScreenshotActions().getLoadScreenshotAlertMessage(), is(equalTo(SUCCESS_MESSAGE)));
        assertThat(testBase.getLoadScreenshotActions().getPublishedScreenshotTitle(), is(equalTo(SCREENSHOT_JPG_TITLE)));
    }

    @Test
    public void uploadNewPdfScreenshotTest() {
        testBase.getNavigationPanelActions()
                .clickOnLoadButton()
                .loadFile(PDF_TEST.getAttachmentAbsolutePath(testBase.getClassLoader()));

        assertThat(testBase.getLoadScreenshotActions().checkIfErrorIsDisplayed(), is(true));
        assertThat(testBase.getLoadScreenshotActions().getErrorMessage(), containsString(LOADING_ERROR_MESSAGE));
    }

    @Test
    public void uploadScreenshotWithoutTitleTest() {
        testBase.getNavigationPanelActions()
                .clickOnLoadButton()
                .loadFile(JPG_TEST.getAttachmentAbsolutePath(testBase.getClassLoader()))
                .clickOnCropAndContinueButton()
                .typeScreenshotTitle(EMPTY)
                .typeScreenShotTags(JPG_TEST.getTags())
                .typeScreenshotDescription(JPG_TEST.getDescription())
                .clickOnPublishDraftButton();

        assertThat(testBase.getLoadScreenshotActions().checkIfErrorIsDisplayed(), is(true));
        assertThat(testBase.getLoadScreenshotActions().getErrorMessage(), containsString(EMPTY_TITLE_ERROR_MESSAGE));
    }
}
