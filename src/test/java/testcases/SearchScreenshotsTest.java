package testcases;



import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static testdata.ConstTestData.*;

public class SearchScreenshotsTest {

    private static TestBase testBase;

    @BeforeClass
    public static void startTest() {
        testBase = new TestBase();
        testBase.setUp();
    }

    @AfterClass
    public static void tearDown() {
        testBase.tearDown();
    }


    @Before
    public void goToMainPage() {
        testBase.getBaseActions().goToMainPage(MAIN_URL);
    }


    @Test
    public void searchScreenshotByTagsTest() {
        testBase.getNavigationPanelActions()
                .findScreenshotsByTags(SEARCHED_TAGS)
                .clickOnFoundedScreenshot();

        assertThat(testBase.getScreenshotViewPageActions().checkIfAuthorIsVisible(), is(true));
        assertThat(testBase.getScreenshotViewPageActions().checkIfTitleIsDisplayed(), is(true));
        assertThat(testBase.getScreenshotViewPageActions().getTags(), hasItems(SEARCHED_TAGS.toArray(new String[SEARCHED_TAGS.size()])));
    }

    @Test
    public void searchScreenshotByAuthorTest() {
        testBase.getNavigationPanelActions()
                .findScreenshotsByOneCriterion(SEARCHED_AUTHOR)
                .clickOnFoundedScreenshot();

        assertThat(testBase.getScreenshotViewPageActions().checkIfAuthorIsVisible(), is(true));
        assertThat(testBase.getScreenshotViewPageActions().getAuthor(), containsString(SEARCHED_AUTHOR));
    }


    @Test
    public void searchScreenshotByTitleTest() {
        testBase.getNavigationPanelActions()
                .findScreenshotsByOneCriterion(SEARCHED_TITLE)
                .clickOnFoundedScreenshot();

        assertThat(testBase.getScreenshotViewPageActions().checkIfTitleIsDisplayed(), is(true));
        assertThat(testBase.getScreenshotViewPageActions().getTitle(), is(equalTo(SEARCHED_TITLE)));
    }

    @Test
    public void searchScreenshotByIncorrectSearchPhrase() {
        testBase.getNavigationPanelActions()
                .findScreenshotsByOneCriterion(SEARCHED_RANDOM_STRING);

        assertThat(testBase.getSearchPageActions().getSearchResultMessage(), containsString(NO_RESULTS_MESSAGE));
    }
}
