package testcases;

import actions.*;


import static testdata.ConstTestData.*;

public class TestBase {

    private ClassLoader classLoader;

    private BaseActions baseActions;

    private NavigationPanelActions navigationPanelActions;

    private LoadScreenshotActions loadScreenshotActions;

    private ScreenshotViewPageActions screenshotViewPageActions;

    private SearchPageActions searchPageActions;

    protected LoadScreenshotActions getLoadScreenshotActions() {
        return loadScreenshotActions;
    }

    protected BaseActions getBaseActions() {
        return baseActions;
    }

    protected NavigationPanelActions getNavigationPanelActions() {
        return navigationPanelActions;
    }

    protected ScreenshotViewPageActions getScreenshotViewPageActions() {
        return screenshotViewPageActions;
    }

    protected SearchPageActions getSearchPageActions() {
        return searchPageActions;
    }

    protected ClassLoader getClassLoader() {
        return classLoader;
    }

    private void init() {
        classLoader = getClass().getClassLoader();
        baseActions = new BaseActions();
        navigationPanelActions = new NavigationPanelActions();
        loadScreenshotActions = new LoadScreenshotActions();
        screenshotViewPageActions = new ScreenshotViewPageActions();
        searchPageActions = new SearchPageActions();
    }

    protected void setUp() {
        init();
        baseActions
                .startTest()
                .maximizeWindow()
                .goToUrl(MAIN_URL);

        navigationPanelActions
                .clickLoginButton()
                .typeUserName(USER_NAME)
                .typePassword(USER_PASSWORD)
                .clickLoginButton();
    }

    protected void tearDown() {
        baseActions.quitTest();
    }
}
