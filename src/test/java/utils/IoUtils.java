package utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class IoUtils {
    public static void copy(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[16384];

        int bytes;
        while(0 < (bytes = in.read(buffer))) {
            out.write(buffer, 0, bytes);
        }
    }
}
