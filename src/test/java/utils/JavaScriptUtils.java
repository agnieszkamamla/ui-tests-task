package utils;

import core.DriverFactory;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

public class JavaScriptUtils {

    public String executeScriptAndReturn(String script) {
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) DriverFactory.getDriver();
        return javascriptExecutor.executeScript(script).toString();
    }

    public void executeScriptWithArgument(String script, WebElement elem){
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) DriverFactory.getDriver();
        javascriptExecutor.executeScript(script, elem);
    }
}
