package utils;

import core.DriverFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class WaitUtils {

    public JavaScriptUtils javaScriptUtils = new JavaScriptUtils();

    public void waitUntilElementIsDisplayed(WebElement element, int timeout) {
        WebDriverWait driverWait = new WebDriverWait(DriverFactory.getDriver(), timeout);
        driverWait.until(ExpectedConditions.visibilityOf(element));
    }

    public void waitUntilElementIsDisplayed(WebElement element) {
        waitUntilElementIsDisplayed(element, 10);
    }

    public void waitUntilElementIsClickable(WebElement element, int timeout) {
        WebDriverWait driverWait = new WebDriverWait(DriverFactory.getDriver(), timeout);
        driverWait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public void waitUntilElementIsClickable(WebElement element) {
        waitUntilElementIsClickable(element, 10);
    }

    public void waitUntilDomIsComplete(int polling, int timeout) {
        ExpectedCondition conditions = (ExpectedCondition<Boolean>) driver -> {
            String script = "return document.readyState";
            return javaScriptUtils.executeScriptAndReturn(script).equals("complete");
        };
        new FluentWait<>(DriverFactory.getDriver()).pollingEvery(Duration.ofSeconds(polling))
                .withTimeout(Duration.ofSeconds(timeout))
                .until(conditions);
    }

    public void waitUntilDomIsComplete() {
        waitUntilDomIsComplete(1, 10);
    }

    public void waitUntilAjaxRequestsAreComplete(int polling, int timeout) {
        ExpectedCondition conditions = (ExpectedCondition<Boolean>) driver -> {
            String script = "return jQuery.active == 0";
            return javaScriptUtils.executeScriptAndReturn(script).equals("true");
        };
        new FluentWait<>(DriverFactory.getDriver()).pollingEvery(Duration.ofSeconds(polling))
                .withTimeout(Duration.ofSeconds(timeout))
                .until(conditions);
    }

    public void waitUntilAjaxRequestsAreComplete() {
        waitUntilAjaxRequestsAreComplete(1, 10);
    }
}

