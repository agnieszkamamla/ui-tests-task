package utils;

import com.google.common.collect.ImmutableList;
import core.DriverFactory;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.List;

public class CommonTestUtils {

    private static Logger logger = LoggerFactory.getLogger(CommonTestUtils.class);

    private static WaitUtils waitUtils = new WaitUtils();

    private static JavaScriptUtils jsUtil = new JavaScriptUtils();

    private static final Duration TIMEOUT = Duration.ofSeconds(15L);

    private static final Duration EXTENDED_TIMEOUT = Duration.ofSeconds(60L);

    private static final Duration POLLING_INTERVAL = Duration.ofMillis(50L);

    private static final List<Class<? extends Exception>> acceptableExceptions = new ImmutableList.Builder<Class<? extends Exception>>()
            .add(NoSuchElementException.class)
            .add(ElementNotVisibleException.class)
            .add(StaleElementReferenceException.class)
            .build();


    public static WebElement findElementWithWait(final By locator) {
        logger.info("Find with wait");
        FluentWait<WebDriver> fluentWait = new FluentWait<WebDriver>(DriverFactory.getDriver());
        fluentWait.pollingEvery(POLLING_INTERVAL)
                .withTimeout(TIMEOUT)
                .ignoreAll(acceptableExceptions);
        return fluentWait.until(webDriver -> webDriver.findElement(locator));
    }

    public static WebElement findElementWithWait(final WebElement elem, final By locator) {
        logger.info("Find with wait");
        FluentWait<WebDriver> fluentWait = new FluentWait<WebDriver>(DriverFactory.getDriver());
        fluentWait.pollingEvery(POLLING_INTERVAL)
                .withTimeout(TIMEOUT)
                .ignoreAll(acceptableExceptions);
        return fluentWait.until(webDriver -> elem.findElement(locator));
    }

    public static List<WebElement> findElementsWithWait(final WebElement elem, final By locator) {
        logger.info("Find with wait");
        FluentWait<WebDriver> fluentWait = new FluentWait<WebDriver>(DriverFactory.getDriver());
        fluentWait.pollingEvery(POLLING_INTERVAL)
                .withTimeout(TIMEOUT)
                .ignoreAll(acceptableExceptions);
        return fluentWait.until(webDriver -> elem.findElements(locator));
    }

    public static List<WebElement> findElementsWithWait(final By locator) {
        logger.info("Find with wait");
        FluentWait<WebDriver> fluentWait = new FluentWait<WebDriver>(DriverFactory.getDriver());
        fluentWait.pollingEvery(POLLING_INTERVAL)
                .withTimeout(TIMEOUT)
                .ignoreAll(acceptableExceptions);
        return fluentWait.until(webDriver -> webDriver.findElements(locator));
    }

    public static void clickElementWithWait(By locator) {
        logger.info("Click on element with wait");
        waitUtils.waitUntilDomIsComplete();
        waitUtils.waitUntilAjaxRequestsAreComplete();
        WebElement elem = findElementWithWait(locator);
        waitUtils.waitUntilElementIsDisplayed(elem);
        findElementWithWait(locator).click();
        waitUtils.waitUntilDomIsComplete();
        waitUtils.waitUntilAjaxRequestsAreComplete();
    }

    public static void clickElementWithWait(WebElement elem) {
        logger.info("Click on element with wait");
        waitUtils.waitUntilDomIsComplete();
        waitUtils.waitUntilAjaxRequestsAreComplete();
        waitUtils.waitUntilElementIsDisplayed(elem);
        waitUtils.waitUntilElementIsClickable(elem);
        marksSelectedElement(elem);
        scrollToElement(elem);
        elem.click();
        waitUtils.waitUntilDomIsComplete();
        waitUtils.waitUntilAjaxRequestsAreComplete();
    }

    public static void sendKeys(WebElement elem, String text) {
        logger.info("Send keys with wait");
        waitUtils.waitUntilDomIsComplete();
        waitUtils.waitUntilAjaxRequestsAreComplete();
        waitUtils.waitUntilElementIsDisplayed(elem);
        marksSelectedElement(elem);
        scrollToElement(elem);
        elem.sendKeys(text);
    }

    public static void sendKeysWithClearField(WebElement elem, String text) {
        logger.info("Send keys with wait");
        waitUtils.waitUntilDomIsComplete();
        waitUtils.waitUntilAjaxRequestsAreComplete();
        waitUtils.waitUntilElementIsDisplayed(elem);
        marksSelectedElement(elem);
        scrollToElement(elem);
        elem.clear();
        elem.sendKeys(text);
    }

    public static void sendKeysWithEnterConfirm(WebElement elem, String text) {
        logger.info("Send keys with wait");
        waitUtils.waitUntilDomIsComplete();
        waitUtils.waitUntilAjaxRequestsAreComplete();
        waitUtils.waitUntilElementIsDisplayed(elem);
        marksSelectedElement(elem);
        scrollToElement(elem);
        elem.clear();
        elem.sendKeys(text);
        elem.sendKeys(Keys.ENTER);
    }

    public static void sendKeysWithWait(By locator, String text) {
        logger.info("Send keys with wait");
        waitUtils.waitUntilDomIsComplete();
        waitUtils.waitUntilAjaxRequestsAreComplete();
        findElementWithWait(locator).sendKeys(text);
    }


    public static void checkIfUrlContains(String text) {
        logger.info("Check page url with wait");
        new FluentWait<>(DriverFactory.getDriver()).pollingEvery(POLLING_INTERVAL)
                .withTimeout(TIMEOUT)
                .until(ExpectedConditions.urlContains(text));
    }

    public static void waitUntilWebElementIsPresent(By locator) {
        logger.info("Check for presence of webelement with wait");
        new FluentWait<>(DriverFactory.getDriver()).pollingEvery(POLLING_INTERVAL)
                .withTimeout(TIMEOUT)
                .ignoreAll(acceptableExceptions)
                .until(ExpectedConditions.presenceOfElementLocated(locator));
    }

    public static void waitUntilWebElementContainsText(WebElement element, String text) {
        logger.info("Check for presence of webelement with wait");
        new FluentWait<>(DriverFactory.getDriver()).pollingEvery(POLLING_INTERVAL)
                .withTimeout(EXTENDED_TIMEOUT)
                .ignoreAll(acceptableExceptions)
                .until(ExpectedConditions.textToBePresentInElement(element, text));
    }

    public static void scrollToElement(WebElement element){
        String script = "arguments[0].scrollIntoView(true)";
        jsUtil.executeScriptWithArgument(script, element);
    }

    public static void marksSelectedElement(WebElement element){
        String script = "arguments[0].style.border='3px solid red'";
        jsUtil.executeScriptWithArgument(script, element);
    }
}
