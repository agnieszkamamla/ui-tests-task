# ui-tests-task

In this repository there are examples of UI automated tests for 2 selected features for `dribble.com` portal. Tests are written in Java with usage of Selenium framework and implementation of Page Object Pattern. 


## Features under test
`dribble.com` platform is a place where artists, graphic and UX/UI designers can promote their work, find inspiration and create community. 
This portal is also dedicated for anyone who is looking for professional designers.
According to me one of the most important functionalities of this application are:

1. uploading a screenshot - for designer this is the key feature to promote work,
2. searching for screenshots by categories - this is one of the most important features for people who are looking for designers or inspiration. 
For both features few test cases have been created with the expected positive results and also with expected failure on invalid action. 
**Notice** that test classes don’t include full coverage for cases. 


Included tests are:

1. uploading a screenshot - in this test class different screenshot types are loaded with different titles,
2. searching for screenshots - by different phrases, empty result lists case is also checked.

## Used tools and plugins
* tests are written in Java, 
* Selenium UI test framework,
* JUnit test framework,
* Hamcrest for assertions,
* Maven Surefire plugin to manage tests

## Project description
Project has Maven project structure. All dependencies are specified in `pom.xml` file. Most of
required properties are also included in the `pom.xml` except `browser.type`, `user.name` and `user.password` properties. 
I have left browser type property empty on purpose to show possible way of handling the case when default browser should be chosen in initialising methods. 
**Notice** that in the project there are only 2 browsers available to select: Chrome and Firefox. I decided to include these browsers because they aren’t specific for selected OS. 
If you want to use another browser, changes in project are required.
User name and user password properties have to be given as JVM options in test run configuration. Test cases are designed for user with basic account (not with Pro account).


Solution shows implementation of Page Object Pattern as well as foundations of test framework to increase readability and keep the classes in order.
There are two test cases: first for uploading screenshots, second for searching for screenshots.
Brief description of project packages (in alphabetical order):

1. actions - in this package we can find ‘action’ classes with methods simulating user behaviour,
2. core - this package includes basic classes with driver configuration,
3. pages - there are classes mapping different ‘pages’ used in test case,
4. testcases - includes test class, 
5. testdata - package with test data,
6. utils - tests utils classes,
7. resources - in this folder additional files are stored

## Run tests

Tests can be run on different OS but there are some preconditions:

1. install JDK in environment (Java Development Kit) -  at least version 1.8.0
2. install Maven in environment ([maven installation guide](https://maven.apache.org/install.html)) - at least 3.3.9
3. download Selenium Standalone Server ([Selenium project page](https://www.seleniumhq.org/download/))
4. install browsers necessary for tests
5. download drivers for all browsers under tests ([Selenium project page](https://www.seleniumhq.org/download/))
6. create directory for storing Selenium Standalone Server and all browser drivers 

I recommend to run tests on Selenium Grid, even if you want to run tests in one environment. To run Selenium Grid:

1. start  terminal/command prompt
2. go to directory with Selenium server and drivers
3. start Selenium hub with command:
`java -jar {selenium-server-standalone-correct_version.jar} -role hub`

4. start another terminal/command prompt window
5. go to directory with Selenium server and drivers
6. start selenium node (or few nodes if you want to) use command (in new terminal/command prompt window):

`java -jar {selenium-server-standalone-correct_version.jar} -role node  -hub http://{selenium.hub.address:port}/grid/register`


**Start tests:**

1. open new terminal/command prompt window
2. go to the directory with test project
3. start tests with command (maven and JDK have to be correctly installed)


`mvn test -Duser.name={user name} -Duser.password={user password}`

Additional options can be provided:


`-Dgrid.url` - to change hub address (default is set to localhost with port 4444)


`-Dbrowser.type` - if you want to run tests not on default browser (by default Chrome browser is started). Notice: in project there are 2 browsers available: Chrome and Firefox. If you want to use another browser project changes are required.


`-Dthreads` - if you want to change number of threads (currently it is set to 2)


It’s also possible to run tests on Docker:

1. install Docker version dedicated for you OS (Docker installation page)
2. start Docker
3. open terminal/command prompt
4. create selenium hub container with command:


`docker run -d -p {port}:{mapped port} --name selenium-hub selenium/hub:{correct_version}`

Example:


`docker run -d -p 4444:4444 --name selenium-hub selenium/hub:latest`


Create selenium node container (or few containers if you want to) with command:


`docker run -d --link selenium-hub:hub selenium/node-{correct_node_type}:{correct_version}`

Example:


`docker run -d --link selenium-hub:hub selenium/node-chrome:latest`

`docker run -d --link selenium-hub:hub selenium/node-firefox:latest`


5. open new terminal/command prompt window
6. go to the directory with test project
7. start tests with command (maven and JDK have to be correctly installed)


`mvn test -Duser.name={user name} -Duser.password={user password}`

The same additional options as previous are available.


## Test report

Tests have been run on Windows OS on my local environment and also on Linux OS with Docker usage.
In test/resources/reports I have attached step by step test documentation from test running.


## Comments and assumptions

1. Login actions are in preconditions step (`@BeforeClass`
annotation). Login functionality should be verified in separated test case. Logout action isn’t necessary for these tests so it’s omitted.
2. User name and password aren’t included in project - these properties are given during start of tests. User appropriate for these tests has basic account (not pro level).
3. Test data such as tags, title or author are declared as constants. In real
project presence of screenshots applicable to this categories should be guaranteed in setup phase (this data should be loaded without usage of Selenium (eg. by API)).
4. `@BeforeClass`, `@Before` and `@AfterClass` are the same in both test classes but it’s done on purpose - set up for all test classes could be different and in project with more test classes preconditions and postconditions steps should be grouped. 
5. In `@Before` step driver goes to main page - from this point all tests are started
6. In utils classes many wait actions are used. This is a precaution against slower
environments. In all cases FluentWait class or explicit waits are used - because of that
thread isn’t freezing and test run time is shorter.
6. I didn’t include more plugins to this project because with only two test classes it’s hard to
show different possibilities. For example in bigger project report plugin (eg. Allure) could be included.
7. Other test framework also could be used. I decided to use Selenium because according to me this tool is most basic one and other frameworks often wrapp it.
8. Additional browser could be added (according to requirements).

